import logging

from nectar_dashboard.rcallocation.models import AllocationRequest
from nectar_dashboard.rcallocation.views import BaseAllocationsListView

LOG = logging.getLogger(__name__)


class MSEAllocationsListView(BaseAllocationsListView):
    page_title = 'Pending Requests (MSE)'

    def get_data(self):
        return (AllocationRequest.objects.all_mse()
                .filter(status__in=(AllocationRequest.NEW,
                                    AllocationRequest.SUBMITTED,
                                    AllocationRequest.UPDATE_PENDING))
                .filter(parent_request=None)
                .order_by('modified_time'))
