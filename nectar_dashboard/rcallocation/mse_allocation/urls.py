from django.conf.urls import url

from nectar_dashboard.rcallocation.mse_allocation import views

urlpatterns = [
    url(r'^$', views.MSEAllocationsListView.as_view(),
        name='mse_allocation_requests'),
]
