from django.utils.translation import ugettext_lazy as _

import horizon
from nectar_dashboard.rcallocation import dashboard


class MSERequests(horizon.Panel):
    name = _("Pending Requests (MSE)")
    slug = 'mse_requests'
    index_url_name = 'mse_allocation_requests'
    permissions = ('openstack.roles.allocationadmin',)


dashboard.AllocationsDashboard.register(MSERequests)
