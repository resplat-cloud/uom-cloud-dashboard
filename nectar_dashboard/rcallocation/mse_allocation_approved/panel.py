from django.utils.translation import ugettext_lazy as _

import horizon
from nectar_dashboard.rcallocation import dashboard


class MSEApprovedRequests(horizon.Panel):
    name = _("Approved Requests (MSE)")
    slug = 'mse_approved_requests'
    index_url_name = 'mse_approved_requests'
    permissions = ('openstack.roles.allocationadmin',)


dashboard.AllocationsDashboard.register(MSEApprovedRequests)
