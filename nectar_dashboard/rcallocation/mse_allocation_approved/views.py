import logging

from nectar_dashboard.rcallocation.models import AllocationRequest
from nectar_dashboard.rcallocation.views import BaseAllocationsListView

LOG = logging.getLogger(__name__)


class MSEApprovedAllocationsListView(BaseAllocationsListView):
    page_title = 'Approved Requests (MSE)'

    def get_data(self):
        mse_allocs = AllocationRequest.objects.all_mse()
        return mse_allocs.filter(parent_request=None).filter(
            status__in=('A', 'X', 'J')).order_by('project_name')
