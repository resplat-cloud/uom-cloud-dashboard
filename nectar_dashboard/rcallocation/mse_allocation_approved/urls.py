from django.conf.urls import url

from nectar_dashboard.rcallocation.mse_allocation_approved import views

urlpatterns = [
    url(r'^$', views.MSEApprovedAllocationsListView.as_view(),
        name='mse_approved_requests'),
]
